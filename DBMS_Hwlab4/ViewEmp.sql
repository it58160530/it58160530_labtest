CREATE VIEW emp AS
     SELECT Employee.Fname,Employee.Lname,Employee.Salary,Department.Dname
     FROM Employee
     LEFT JOIN Department ON Employee.EmpID=Department.Dnumber
     ORDER BY Employee.Salary ASC; 
