CREATE VIEW management AS
  SELECT Department.Dnumber,Department.Dname,Manager.Fname,Manager.Lname
  FROM Department
  LEFT JOIN Manager
  ON Department.Dnumber=Manager.MNumber
  ORDER BY Department.Dname ASC;
